% sshtunnel(1) | reverses-ssh manpage

NAME
====

**sshtunnel** - a simple **ssh** wrapper to create tunnels

SYNOPSIS
========

**sshtunnel** HOST - connect to HOST

DETAILS
=======

Creates an SSH tunnel to a preconfigured host. Uses the user's default SSH
configuration (see **ssh_config (5)**), unless running as the **sshtunnel**
user, in which case the SSH configuration file `/etc/sshtunnel/config` is
used.

USER
====

A system user **sshtunnel** is automatically created during installation. Its
default SSH key is automatically generated during installation and is located
at `/var/lib/sshtunnel/.ssh/id_rsa`. The **sshtunnel** user doesn't have a
login shell.

SYSTEMD UNIT
============

A systemd template unit file `sshtunnel@.service` is also shipped.

You can start a tunnel to a preconfigured host **HOST** via

```bash
# systemctl start sshtunnel@HOST.service
```

Make it start at boot:

```bash
# systemctl enable sshtunnel@HOST
```

Stop the tunnel:

```bash
# systemctl stop sshtunnel@HOST
```

Inspect with:

```bash
$ systemctl status sshtunnel@HOST
$ journalctl -ef --unit sshtunnel@HOST
```

The SSH configuration file with the configured HOSTS is located at
`/etc/sshtunnel/config` (see **ssh_config (5)** for further information).


CONFIGURATION
=============

> ### Note
>
> Key-based authentication is recommended. Add the **sshtunnel** user's SSH
> key from `/var/lib/sshtunnel/.ssh/id_rsa.pub` to the
> `~/.ssh/authorized_keys` file of the user on the server you're connecting to:
>
> ```
> ...
> ssh-rsa AAAAB3NzaC1yc2EAA...WcA0pXpL sshtunnel@yourhostname
> ```

The client configuration file `/etc/sshtunnel/config` shipped with this package
sets some options that are useful for port forwarding applications.

Consider you'd like to open port 1234 on `example.com` through so you can **ssh
(1)** back to your machine. You'd then add the following lines to
`/etc/sshtunnel/config`:


```
Host example
  HostName example.com
  User user_on_example
  RemoteForward 1234 localhost:22
```

Start the tunnel via systemd:

```
# systemctl start sshtunnel@example
```

Or start the tunnel directly:

```
# su sshtunnel -s /bin/sh sshtunnel example
```

ENVIRONMENT VARIABLES
=====================

The **SSH_OPTS** environment variable can be used to specify further arguments
to **ssh (1)**. It defaults to **-nN**.

TIPS
====

You can also install **sshtunnel** on the server side and let incoming port
forwarding connections connect via the **sshtunnel** user.

Read **sshd (8)** to learn how to restrict the ports to forward on the remote
server via the `authorized_keys` file.

AUTHOR
======

Yann Büchau <nobodyinperson@posteo.de>


SEE ALSO
========

## SSH

- ssh (1)
- sshd (8)
- ssh_config (5)
- sshd_config (5)

## SYSTEMD

- systemd (1)
- systemctl (1)
